//1. Définir la valeur d'une zone de saisie

//2. Retourner en haut de la page

//3. Désactiver le menu contextuel sur une page HTML

//4. Désactiver le bouton de soumission de formulaire tant qu'un utilisateur n'a pas coché une case

//5. Faire clignoter un texte

//6. Déplacer un élément "div" dans un autre

//7. Ajouter les éléments d'une liste (JS) dans une liste non ordonée (HTML)

//8. Récupérer la valeur d'une zone de saisie

//9. Enlever toutes les classes CSS

//10. Distinguer un clic droit d'un clic gauche (évènement)

//11. Enlever l'attribut "disabled"

//12. Détecter si le contenu d'une zone de saisie a changé