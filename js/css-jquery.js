jQuery(function () {
    $('#getValeurProp').on('click', function () {
        $('#resGetValeurProp').html($(this).css('color'));
    });

    $('#setValeurPropRed').on('click', function () {
        $('#resSetValeurProp').css('color', 'red');
    });

    $('#setValeurPropBlack').on('click', function () {
        $('#resSetValeurProp').css('color', 'black');
    });

    $('#animation01').on('click', () => {
        $('#blocAnimate01').animate({
            width: "250px",
            fontSize: ["2rem", "linear"]
        },
        "slow", //slow : 600ms ; normal : 400 ms ; fast : 200 ms
        () => {
            console.log('animation 1 (swing) terminee')
        });

        $('#blocAnimate02').animate({
            width: "250px",
            fontSize: "2rem"
        },
        "slow", //slow : 600ms ; normal : 400 ms ; fast : 200 ms
        "linear", //easing
        () => {
            console.log('animation 1 (linear) terminee')
        });
    });

    $('#animation01Reset').on('click', () => {
        $('#blocAnimate01').animate({
            width: "50px",
            fontSize: ["1rem", "linear"]
        },
        "slow", //slow : 600ms ; normal : 400 ms ; fast : 200 ms
        () => {
            console.log('animation 1 (swing) terminee')
        });

        $('#blocAnimate02').animate({
            width: "50px",
            fontSize: "1rem"
        },
        "slow", //slow : 600ms ; normal : 400 ms ; fast : 200 ms
        "linear", //easing
        () => {
            console.log('animation 1 (linear) terminee')
        });
    });

    $('#afficher').on('click', () => {
        $('#troisEtats').animate({
            width: "show"
        });
        // équivalent à
        $('#troisEtats').show();
    });

    $('#cacher').on('click', () => {
        $('#troisEtats').animate({
            width: "hide"
        });
        // équivalent à
        $('#troisEtats').hide();
    });

    $('#inverser').on('click', () => {
        $('#troisEtats').animate({
            width: "toggle"
        });
        // équivalent à
        $('#troisEtats').toggle();
    });

    $('#changerCouleur').on('click', function () {
        let bloc = $('#troisEtats');

        if (bloc.hasClass("blue")) {
            /* - possibilité de supprimer plusieurs classes en les séparant par des espaces ("blue important")
               - pour supprimer toutes les classes : .removeClass(); */
            bloc.removeClass("blue");
        } else {
            bloc.addClass("blue"); // possibilité d'ajouter plusieurs classes en les séparant par des espaces ("blue important")
        }

        // en JS ça donnerait

        let blocJS = document.getElementById("troisEtats");

        if (blocJS.classList.contains("blue")) {
            blocJS.classList.remove("blue"); // supprimer plusieurs classes : .remove("blue", "important");
        } else {
            blocJS.classList.add("blue"); // ajouter plusieurs classes : .add("blue", "important");
        }
    })
});