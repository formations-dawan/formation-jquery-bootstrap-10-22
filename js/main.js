jQuery(document).ready(function () {
    console.log("DOM prêt");
});

jQuery(function () {
    console.log("DOM prêt");
});

$(function() {
    console.log("DOM prêt");
});



jQuery(function () {
    $('#firstBlood').click(function () {
        console.log("Une personne a cliqué sur le bouton First Blood");
    });
    //en JS ça donnerait
    document.querySelector('#firstBlood').addEventListener('click', function () {
        //...
    });

    $('#firstBlood').on('click', function () {
        //...
    });

    let cptClicP = 0;
    let cptClicPSpecial = 0;

    $('#activerCompteurs').click(function () {
        $('p:not(.special)').on('click', function () {
            cptClicP++;
            console.log("compteur p pas special : " + cptClicP);
        });

        //en JS ça donnerait

        document.querySelectorAll('p:not(.special)').forEach(p => {
            p.addEventListener('click', function () {
                cptClicP++;
                console.log("compteur p pas special : " + cptClicP);
            });
        });
        //////////////////////////////////////
        $('p.special').on('click', function () {
            cptClicPSpecial++;
            console.log("compteur p special : " + cptClicPSpecial);
        });
    });

    $('.divLettreCercle').first().on('click', function () {
        console.log('A');
    });

    $('.divLettreCercle').last().on('click', function () {
        console.log('C');
    });

    $('.divLettreCercle').eq(1).on('click', function () {
        console.log('B');
    });

    $('p .lien').css('color', 'red');

    $('#nom').on('focus', function () {
        console.log("Nom a le focus");
    });

    $('#nom').on('keyup', function () {
        console.log("On saisie dans nom");
    });

    $('input[type=submit]').on('click', function (e) {
        e.preventDefault();

        let nom = $('#nom').val();
        let prenom = $('#prenom').val();

        if ("" !== nom && "" !== prenom) {
            console.log("Prenom / Nom OK");
            $('#formContact').submit();
        } else {
            console.log("Prenom / Nom pas OK");
        }
    });

    $('#clickVirtuel').on('click', function () {
        console.log("clique sur bouton désactivé");
    });

    $('#clickVirtuel').trigger('click');

    $('a.retourHaut').on('click', function (e) {
        e.preventDefault();

        $('html, body').animate({scrollTop: 0}, 1000);
        //$('html, body').scrollTop(0);
    });

    $('a.allerBas').on('click', function (e) {
        e.preventDefault();

        $('html, body').scrollTop($(document).height());
    });

    $('#formContact').append('<input id="test" name="test" type="text">');

    $('#test').val("Bonjour");

    // Différence entre .attr et .prop
    $('#aTest').prop('href', 'css-jquery.html'); // On modifie le DOM
    $('#inTexteTest').attr('value', "Dawan"); // On modifie l'attribut HTML (non recommandé)

    console.log($('#inTexteTest').val());

    $('#inCbTest').prop('checked', true);
    // En général utiliser prop au lieu de attr


    // remove() enlève complètement l'élément ; il disparaît de la page et du DOM
    // detach() enlève l'élément de la page mais le conserve dans le DOM pour être réutilisé
    $('#div1').detach().appendTo($('#div2').parent());
    $('#div2').detach().appendTo('#div1');
});
