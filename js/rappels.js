var prenom = "Antoine";
let x = 25;
const MA_CONSTANTE = "Coucou";

let test;

function monAge(test) {
    console.log("mon age est" + test);
}

monAge(x);

console.log(2 ** 3);
console.log(Math.pow(2, 3));

console.log(x++);

let y;

if (!y) {
    console.log("test");
}

let voitures = ["Peugeot", "Ford", "Renault"];

let v2 = voitures[1];

for (indice in voitures) {
    let v = voitures[indice];

    console.log(`voitures[${indice}] = ${v}`);
}

for (v of voitures) {
    console.log(v);
}

voitures.forEach(function (v, indice) {
    console.log(`voitures[${indice}] = ${v}`);
});

voitures.push("Hyundai");

/*  Déréférencement du tableau (conversion du tableau en plusieurs variables,
    chacune contenant 1 élément du tableau)
*/
let [p, f, r, h] = voitures;
console.log(p); // Peugeot

// Fonction = méthode qui retourne une valeur
function fonction() {
    let nom = "DELARUE";

    return nom;
}

// Procédure = méthode qui ne retourne pas de valeur
function procedure() {
    let majeur = true;
    
    console.log(majeur);
}

console.log(fonction());

procedure();


function documentCharge (retourFonction) {
    window.addEventListener("DOMContentLoaded", retourFonction);
}

function recuperer (element) {
    return document.querySelector(element);
}

documentCharge(function () {
    console.log("DOM prêt");

    let btn = recuperer('#btn-ok');
    btn.addEventListener('click', function () {
        console.log("bouton cliqué");
    });

    let paragraphes = document.querySelectorAll('p');

    paragraphes.forEach(p => console.log(p.innerText));
});